@extends ('layouts.admin')
@section ('contenido')
<div class="row">
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
    <h3>Lista de Usuarios <a href="usuarios/create"><button class="btn btn-success">Nuevo</button></a></h3>     
    @include('usuarios.search')
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-condensed table-hover">
        <thead>          
          <th>Nombre</th>
          <th>Apellidos</th>
          <th>Email</th>
          <th>Telefono</th>
          <th>Direccion</th>
          <th>Redes</th>
          <th>Opciones</th>
        </thead>
        @foreach ($usuarios as $user)
        <tr>          
          <td>{{$user->name}}</td>
          <td>{{$user->apellidos}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->telefono}}</td>
          <td>{{$user->direccion}}</td>
          <td>{{$user->redes}}</td>
          <td>
            <a href="{{URL::action('UsuarioController@edit',$user->id)}}"><button class="btn btn-info">Editar</button></a>
            @if (auth()->user()->id != $user->id)
            <a href="" data-target="#modal-delete-{{$user->id}}" data-toggle="modal" ><button class="btn btn-danger">Eliminar</button></a>
            @endif
          </td>
        </tr>
        @include('usuarios.modal')
        @endforeach
      </table>
    </div>
    {{$usuarios->render()}}
  </div>
</div>
@stop