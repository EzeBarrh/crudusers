@extends ('layouts.admin')
@section ('contenido')
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <h3>Edicion de Usuario {{$usuario->name}}</h3>
      @if (count($errors) >0 )
      <div class="alert alert-danger">        
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
      @endif

      {!! Form::model($usuario,['method'=>'PATCH','route'=>['usuarios.update',$usuario->id]])!!}
      {{Form::token()}}
      <div class="form-group">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" class="form-control" value="{{$usuario->name}}" placeholder="Nombre"/>
      </div>
      <div class="form-group">
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" class="form-control" value="{{$usuario->apellidos}}" placeholder="Apellidos"/>
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control" value="{{$usuario->email}}" placeholder="Email"/>
      </div>      
      <div class="form-group">
        <label for="telefono">Telefono</label>
        <input type="text" name="telefono" class="form-control" value="{{$usuario->telefono}}" placeholder="Telefono"/>
      </div>
      <div class="form-group">
        <label for="direccion">Direccion</label>
        <input type="text" name="direccion" class="form-control" value="{{$usuario->direccion}}" placeholder="Direccion"/>
      </div>
      <div class="form-group">
        <label for="redes">Redes</label>
        <input type="text" name="redes" class="form-control" value="{{$usuario->redes}}" placeholder="Redes"/>
      </div>
      <div class="form-group">
        <button class="btn btn-primary" type="submit">Guardar</button>
        <button class="btn btn-danger" type="reset">Cancelar</button>
      </div>
      {!!Form::close() !!}
    </div>
  </div>
@endsection