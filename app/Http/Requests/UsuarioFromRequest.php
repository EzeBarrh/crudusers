<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class UsuarioFromRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:50',
            'apellidos' => 'required|max:100',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],            
            'telefono' => 'required|max:20',
            'direccion' => 'required|max:100',
            'redes' => 'max:100'
        ];
    }
}
