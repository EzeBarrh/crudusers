<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () { 
    $isAuthorized = Auth::check();   
    if(!$isAuthorized){
        return view('auth.login');    
    }else{
       return redirect('usuarios');
    }
    
});


Auth::routes();


Route::resource('usuarios', 'UsuarioController');
