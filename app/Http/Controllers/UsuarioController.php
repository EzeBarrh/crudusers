<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Http\Requests\UsuarioFromRequest as RequestsUsuarioFormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function __construct()
    {
        
    }

    public function index(Request $request){        
        if($request){
            $query=trim($request->get('searchText'));
            $usuarios= DB::table('users')-> where('name','LIKE','%'.$query.'%') 
            ->orderBy('name','asc')
            ->paginate(7);
            return view('usuarios.index',["usuarios"=>$usuarios,"searchText"=>$query]);
        }else{
            $usuarios= DB::table('users') 
            ->orderBy('name','asc')
            ->paginate(7);
            return view('usuarios.index',["usuarios"=>$usuarios]);
        }
    }

    public function create(){
        return view("usuarios.create");
    }

    public function store(RequestsUsuarioFormRequest $request){
        $usuario=New Usuario;
        $usuario -> name= $request->get('nombre');
        $usuario -> apellidos= $request->get('apellidos');
        $usuario -> email= $request->get('email');
        $usuario -> password= Hash::make($request->get('password'));
        $usuario -> telefono= $request->get('telefono');
        $usuario -> direccion= $request->get('direccion');
        $usuario -> redes= $request->get('redes');
        $usuario -> save();
        return redirect('usuarios');
    }

    public function show($id){
        return view('usuarios.show',["usuario"=> Usuario::findOrFail($id)]);
    }

    public function edit($id){
        return view('usuarios.edit',["usuario"=> Usuario::findOrFail($id)]);
    }

    public function update(RequestsUsuarioFormRequest $request,$id){
        $usuario= Usuario::findOrFail($id);
        $usuario->name=$request->get("nombre");
        $usuario->apellidos=$request->get("apellidos");
        $usuario->telefono=$request->get("telefono");
        $usuario->direccion=$request->get("direccion");
        $usuario->redes=$request->get("redes");
        $usuario->update();
        return redirect('usuarios');
    }

    public function destroy($id){
        $usuario=Usuario::findOrFail($id);
        $usuario->delete();
        return redirect('usuarios');
    }
}
