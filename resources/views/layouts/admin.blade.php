<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Crud Usuarios</title>    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('/public/css/bootstrap.min.css')}}">    
    <link rel="stylesheet" href="{{asset('/public/css/font-awesome.css')}}">    
    <link rel="stylesheet" href="{{asset('/public/css/AdminLTE.min.css')}}">             
    <link rel="stylesheet" href="{{asset('/public/css/_all-skins.min.css')}}">
    <link rel="apple-touch-icon" href="{{asset('/public/img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('/public/img/favicon.ico')}}">

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        
        <a href="#" class="logo">          
          <span class="logo-mini"><b>C</b>U</span>          
          <span class="logo-lg"><b>Crud Usuarios</b></span>
        </a>
        
        <nav class="navbar navbar-static-top" role="navigation">          
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">            
          </a>          
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">              
                            
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <small class="bg-red">Online</small>
                  <span class="hidden-xs"> {{ auth()->user()->name}}</span>
                </a>
                <ul class="dropdown-menu">                  
                  <li class="user-header">
                    
                    <p>                      
                      <small>Red</small>
                    </p>
                  </li>
                                    
                  <li class="user-footer">
                    
                    <div class="pull-right">
                      <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">{{ __('Logout') }}</a>                                                             
                                    </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                      </form>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>

        </nav>
      </header>      
      <aside class="main-sidebar">        
        <section class="sidebar">          
                              
          <ul class="sidebar-menu">
            <li class="header"></li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Menu</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>              
              <ul class="treeview-menu">
                <li><a href="{{ url('/') }}"><i class="fa fa-circle-o"></i> Usuarios</a></li>
                
              </ul>
            </li>
             <li>
              <a href="#">
                <i class="fa fa-plus-square"></i> <span>Ayuda</span>
                <small class="label pull-right bg-red">PDF</small>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-info-circle"></i> <span>Acerca De...</span>
                <small class="label pull-right bg-yellow">IT</small>
              </a>
            </li>
                        
          </ul>
        </section>        
      </aside>




             
      <div class="content-wrapper">
                
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Crud Usuarios</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>                
                <div class="box-body">
                  	<div class="row">
	                  	<div class="col-md-12">
		                          <!--Contenido-->
                              @yield('contenido')
		                          <!--Fin Contenido-->
                           </div>
                        </div>
		                    
                  		</div>
                  	</div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section>
      </div>      
      <footer class="main-footer">
        <div class="pull-right hidden-xs">          
        </div>        
      </footer>

      
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
    
  </body>
</html>
